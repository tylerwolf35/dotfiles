# Dotfiles
## my system config!

![screenshot](screenshot.png)

**Operating System:** [GNU Guix](https://guix.gnu.org/)

**Desktop Environment:** [Xfce](https://xfce.org/)

**Window Manager:** [xmonad](https://xmonad.org/)

**Text Editor:** [GNU Emacs](https://www.gnu.org/software/emacs/)

**Terminal:** [Kitty](https://sw.kovidgoyal.net/kitty/)

**Font:** [Iosevka Term](https://typeof.net/Iosevka/)

**Theme:** [Kanagawa](https://github.com/rebelot/kanagawa.nvim)
